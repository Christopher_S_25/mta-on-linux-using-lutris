# MTA on Linux using Lutris

I've also uploaded this guide on the [MTA Wiki](https://wiki.multitheftauto.com/wiki/Client_on_Linux_using_Lutris_Manual)

## Requirements

* Basic knowledge about the [Wine Prefix](https://linuxconfig.org/using-wine-prefixes)
* [Lutris](https://lutris.net/downloads/)
* Lutris Wine Runner: `lutris-6.1-3`
* A prefix with GTA:SA V1.0 installation (Lutris can be used)
* [MTA Installer](https://www.mtasa.com/)

## Installing MTA

1. On Lutris, `Add(+)` a Game:
    * Game Info &rarr; Name: `Multi Theft Auto`
    * Game Options &rarr; Wine Prefix: **Same as the GTA:SA installation prefix**
    * Runner Options &rarr; Wine Version: `lutris-6.1-3`
    * Runner Options &rarr; Enable DXVK/VKD3D: `Disabled`
2. On Lutris, click on `Multi Theft Auto` and on the bottom bar, click on the Wine popup menu and select `Run EXE inside Wine prefix`
    * On the File Manager that appears, find and select the MTA Setup executable that you downloaded
3. Run the setup normally, defaults should be working fine
    * Before finishing setup, make sure you **untick** `Run MTA`
4. On Lutris, right click `Multi Theft Auto` and select `Configure`
    * Game Options &rarr; Executable: Should point to `Multi Theft Auto.exe` inside the `Wine Prefix` (eg. "{`Wine Prefix`}/drive_c/Program Files (x86)/MTA San Andreas 1.5/Multi Theft Auto.exe")
5. On Lutris, click on `Multi Theft Auto` and on the bottom bar, click on the Wine popup menu and select
`Winetricks`
    * Select the default wineprefix &rarr; Install a font &rarr; Check `Tahoma` and `Verdana`
6. On Lutris, launch `Multi Theft Auto`
    * You should be greeted with an "Error serial", but that's actually a good sign, continue to solve it
7. In order to solve the "Error serial", [download and run the native MTA Server](https://wiki.multitheftauto.com/wiki/Server_Manual#Linux_installation)
    * **After about a minute running the server**, close it
8. On Lutris, launch `Multi Theft Auto`
    * MTA should launch without any issues, as well as successfully connect to servers

## Tips

* This setup works with MTA 1.5.8 (tested)
* There are probably some Lutris Scripts available for installing MTA, but doing it manually should work better (untested)
* Ignore any Wine Mono and Wine Gecko installation prompts, as they are not needed for MTA (tested)
* The [`wine-tkg 6.1`](https://github.com/Frogging-Family/wine-tkg-git/releases/tag/6.1.r0.gf6dacd2f) version works and can be used instead of the Lutris one (tested)
* For audio streaming, installing `wmp10` using winetricks from Lutris might help (untested)

## Known Issues

* Using any other Wine version other than 6.1 might result to:
    * "SD #16 Protocol Error" when connecting to a server, or
    * "No audio card detected" when launching either GTA:SA or MTA
* Using standard Full-screen mode on MTA might cause some occasional artifacts
* Enabling DXVK might not break GTA:SA, but it breaks MTA
* Some crashes might occur on some servers for various reasons


